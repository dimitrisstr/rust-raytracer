use core::scene_parser::SceneParser;
use std::fs::File;
use std::process::exit;
use std::{env, fs};

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];

    match SceneParser::load(&fs::read_to_string(filename).unwrap()) {
        Ok((camera, scene)) => {
            let canvas = camera.render(&scene);
            let file = File::create("image.ppm").expect("File creation failed.");
            canvas.to_ppm(file).expect("Image creation failed.");
        }
        Err(e) => {
            eprintln!("Parse failed: {}", e);
            exit(1)
        }
    }
}
