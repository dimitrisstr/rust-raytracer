//! # Properties
//!
//! This crate implements the Color and the Material types.

use math::utils;
use std::fmt;

pub const BLACK: Color = Color {
    r: 0.,
    g: 0.,
    b: 0.,
};

pub const WHITE: Color = Color {
    r: 1.,
    g: 1.,
    b: 1.,
};

/// RGB Color type. The value of each field is between 0 and 1, with 0 meaning the color is
/// entirely absent and 1 meaning the color is fully present.
#[derive(Copy, Clone, Debug)]
pub struct Color {
    /// Red.
    pub r: f64,
    /// Green.
    pub g: f64,
    /// Blue.
    pub b: f64,
}

impl Color {
    pub fn new(r: f64, g: f64, b: f64) -> Color {
        Self { r, g, b }
    }
}

impl From<Color> for (u8, u8, u8) {
    /// Converts a Color to a RGB tuple.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let c = Color { r: 0.2, g: 1.5, b: 0.501 };
    /// let a: (u8, u8, u8) = <(u8, u8, u8)>::from(c);
    ///
    /// assert_eq!(a, (51, 255, 127));
    /// ```
    fn from(color: Color) -> Self {
        (
            (color.r * 255.).clamp(0., 255.) as u8,
            (color.g * 255.).clamp(0., 255.) as u8,
            (color.b * 255.).clamp(0., 255.) as u8,
        )
    }
}

impl From<(u8, u8, u8)> for Color {
    /// Converts a RGB tuple to a Color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let a = Color::from((51, 255, 128));
    ///
    /// assert_eq!(a, Color { r: 0.2, g: 1., b: 0.50196 });
    /// ```
    fn from(rgb: (u8, u8, u8)) -> Self {
        Self {
            r: rgb.0 as f64 / 255.,
            g: rgb.1 as f64 / 255.,
            b: rgb.2 as f64 / 255.,
        }
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let tuple: (u8, u8, u8) = (*self).into();
        write!(f, "{} {} {}", tuple.0, tuple.1, tuple.2)
    }
}

impl std::ops::Add<Color> for Color {
    type Output = Color;

    /// Adds two colors and returns a new color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let a = Color { r: 0.2, g: 0.3, b: 0.7 };
    /// let b = Color { r: 0.5, g: 0.1, b: 0.05 };
    ///
    /// assert_eq!(a + b, Color { r: 0.7, g: 0.4, b: 0.75 });
    /// ```
    fn add(mut self, rhs: Color) -> Self::Output {
        self += rhs;
        self
    }
}

impl std::ops::AddAssign<Color> for Color {
    /// Adds a color to a color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let mut a = Color { r: 0.2, g: 0.3, b: 0.7 };
    /// a += Color { r: 0.5, g: 0.1, b: 0.05 };
    ///
    /// assert_eq!(a, Color { r: 0.7, g: 0.4, b: 0.75 });
    /// ```
    fn add_assign(&mut self, rhs: Color) {
        self.r += rhs.r;
        self.g += rhs.g;
        self.b += rhs.b;
    }
}

impl std::ops::Sub<Color> for Color {
    type Output = Color;

    /// Subtracts two colors and returns a new color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let a = Color { r: 0.8, g: 0.3, b: 0.7 };
    /// let b = Color { r: 0.5, g: 0.1, b: 0.05 };
    ///
    /// assert_eq!(a - b, Color { r: 0.3, g: 0.2, b: 0.65 });
    /// ```
    fn sub(mut self, rhs: Color) -> Self::Output {
        self -= rhs;
        self
    }
}

impl std::ops::SubAssign<Color> for Color {
    /// Subtracts a color from a color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let mut a = Color { r: 0.8, g: 0.3, b: 0.7 };
    /// a -= Color { r: 0.5, g: 0.1, b: 0.05 };
    ///
    /// assert_eq!(a, Color { r: 0.3, g: 0.2, b: 0.65 });
    /// ```
    fn sub_assign(&mut self, rhs: Color) {
        self.r -= rhs.r;
        self.g -= rhs.g;
        self.b -= rhs.b;
    }
}

impl std::ops::Mul<f64> for Color {
    type Output = Color;

    /// Multiplies a color by a scalar and returns a new color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let a = Color { r: 0.2, g: 0.4, b: 0.1 };
    ///
    /// assert_eq!(a * 2., Color { r: 0.4, g: 0.8, b: 0.2 });
    /// ```
    fn mul(mut self, rhs: f64) -> Self::Output {
        self *= rhs;
        self
    }
}

impl std::ops::MulAssign<f64> for Color {
    /// Multiplies a color by a scalar.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let mut a = Color { r: 0.2, g: 0.4, b: 0.1 };
    /// a *= 2.;
    ///
    /// assert_eq!(a, Color { r: 0.4, g: 0.8, b: 0.2 });
    /// ```
    fn mul_assign(&mut self, rhs: f64) {
        self.r *= rhs;
        self.g *= rhs;
        self.b *= rhs;
    }
}

impl std::ops::Mul<Color> for Color {
    type Output = Color;

    /// Multiplies two colors and returns a new color.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let a = Color { r: 0.2, g: 0.4, b: 0.5 };
    /// let b = Color { r: 1., g: 0.5, b: 0.7 };
    ///
    /// assert_eq!(a * b, Color { r: 0.2, g: 0.2, b: 0.35 });
    /// ```
    fn mul(mut self, rhs: Color) -> Self::Output {
        self *= rhs;
        self
    }
}

impl std::ops::MulAssign<Color> for Color {
    /// Multiplies a vector by a vector.
    ///
    /// ```
    /// # use core::properties::Color;
    /// let mut a = Color { r: 0.2, g: 0.4, b: 0.5 };
    /// a *= Color { r: 1., g: 0.5, b: 0.7 };
    ///
    /// assert_eq!(a, Color { r: 0.2, g: 0.2, b: 0.35 });
    /// ```
    fn mul_assign(&mut self, rhs: Color) {
        self.r *= rhs.r;
        self.g *= rhs.g;
        self.b *= rhs.b;
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        utils::eq(self.r, other.r) && utils::eq(self.g, other.g) && utils::eq(self.b, other.b)
    }
}

#[derive(Debug, PartialEq)]
pub struct Material {
    pub ambient: f64,
    pub diffuse: f64,
    pub specular: f64,
    pub shininess: f64,
    pub reflective: f64,
    pub transparency: f64,
    pub refractive_index: f64,
}

impl Default for Material {
    fn default() -> Self {
        Self {
            ambient: 0.1,
            diffuse: 0.9,
            specular: 0.9,
            shininess: 200.,
            reflective: 0.,
            transparency: 0.,
            refractive_index: 1.,
        }
    }
}
