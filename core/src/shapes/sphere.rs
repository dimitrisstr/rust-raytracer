use crate::properties::{Material, WHITE};
use crate::ray::Ray;
use crate::shapes::shape::ShapeComputations;
use crate::shapes::{IntersectionRecorder, Shape, ShapeProperties};
use math::{Point4f, Vector4f};

/// A sphere that is centered at the world origin (0, 0, 0) and has a radius of 1.
pub struct Sphere {
    properties: ShapeProperties,
}

impl Sphere {
    /// Creates a new sphere.
    pub fn new(properties: ShapeProperties) -> Self {
        Self { properties }
    }
}

impl Default for Sphere {
    fn default() -> Self {
        Sphere::new(ShapeProperties::new(Box::new(WHITE), Material::default()))
    }
}

impl Shape for Sphere {}

impl ShapeComputations for Sphere {
    fn compute_intersections<'a>(
        &'a self,
        ray: &Ray,
        intersections: &mut IntersectionRecorder<'a>,
    ) {
        let direction = ray.get_direction();
        let sphere_to_ray = ray.get_origin() - &Point4f::new(0., 0., 0.);
        let a = direction.dot(direction);
        let b = 2. * direction.dot(&sphere_to_ray);
        let c = sphere_to_ray.dot(&sphere_to_ray) - 1.;
        let discriminant = b * b - 4. * a * c;

        if discriminant < 0. {
            return;
        }

        let discriminant_sqrt = discriminant.sqrt();
        intersections.add(self, (-b - discriminant_sqrt) / (2. * a));
        intersections.add(self, (-b + discriminant_sqrt) / (2. * a));
    }

    fn compute_normal(&self, point: &Point4f) -> Vector4f {
        Vector4f::new(point.x, point.y, point.z)
    }

    fn get_properties(&self) -> &ShapeProperties {
        &self.properties
    }
}

#[cfg(test)]
mod tests {
    use crate::properties::{Material, BLACK};
    use crate::ray::Ray;
    use crate::shapes::{IntersectionRecorder, Shape, ShapeProperties, Sphere};
    use math::{Matrix4f, Point4f, Vector4f};
    use std::f64::consts::FRAC_1_SQRT_2;

    /// A ray intersects a sphere at two points.
    #[test]
    fn intersects_two_points() {
        let mut intersection_recorder = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::default();
        sphere.intersect(&ray, &mut intersection_recorder);
        let intersections: Vec<f64> = intersection_recorder
            .sorted()
            .iter()
            .map(|intersection| intersection.t)
            .collect();

        assert_eq!(intersections, vec![4., 6.]);
    }

    /// A ray intersect a sphere at a tangent.
    #[test]
    fn intersects_tangent() {
        let mut intersection_recorder = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 1., -5.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::default();
        sphere.intersect(&ray, &mut intersection_recorder);
        let intersections: Vec<f64> = intersection_recorder
            .sorted()
            .iter()
            .map(|intersection| intersection.t)
            .collect();

        assert_eq!(intersections, vec![5., 5.]);
    }

    /// A ray originates inside a sphere.
    #[test]
    fn intersects_inside() {
        let mut intersection_recorder = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::default();
        sphere.intersect(&ray, &mut intersection_recorder);
        let intersections: Vec<f64> = intersection_recorder
            .sorted()
            .iter()
            .map(|intersection| intersection.t)
            .collect();

        assert_eq!(intersections, vec![-1., 1.]);
    }

    /// A sphere is behind a ray.
    #[test]
    fn intersects_behind() {
        let mut intersection_recorder = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 0., 5.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::default();
        sphere.intersect(&ray, &mut intersection_recorder);
        let intersections: Vec<f64> = intersection_recorder
            .sorted()
            .iter()
            .map(|intersection| intersection.t)
            .collect();

        assert_eq!(intersections, vec![-6., -4.])
    }

    /// A ray misses a sphere.
    #[test]
    fn intersects_miss() {
        let mut intersections = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 2., -5.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::default();
        sphere.intersect(&ray, &mut intersections);

        assert_eq!(intersections.len(), 0);
    }

    /// A ray intersects a scaled sphere.
    #[test]
    fn intersects_scale() {
        let mut intersection_recorder = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::new(ShapeProperties::with_transform(
            Box::new(BLACK),
            Material::default(),
            Matrix4f::identity().scale(2., 2., 2.),
        ));
        sphere.intersect(&ray, &mut intersection_recorder);
        let intersections: Vec<f64> = intersection_recorder
            .sorted()
            .iter()
            .map(|intersection| intersection.t)
            .collect();

        assert_eq!(intersections, vec![3., 7.]);
    }

    /// A ray intersects a translated sphere.
    #[test]
    fn intersects_translation() {
        let mut intersections = IntersectionRecorder::new();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));
        let sphere = Sphere::new(ShapeProperties::with_transform(
            Box::new(BLACK),
            Material::default(),
            Matrix4f::identity().translate(5., 0., 0.),
        ));

        sphere.intersect(&ray, &mut intersections);

        assert_eq!(intersections.len(), 0);
    }

    /// The normal of a sphere.
    #[test]
    fn normal() {
        let sphere = Sphere::default();

        assert_eq!(
            sphere.normal_at(&Point4f::new(1., 0., 0.)),
            Vector4f::new(1., 0., 0.)
        );
        assert_eq!(
            sphere.normal_at(&Point4f::new(0., 1., 0.)),
            Vector4f::new(0., 1., 0.)
        );
        assert_eq!(
            sphere.normal_at(&Point4f::new(0., 0., 1.)),
            Vector4f::new(0., 0., 1.)
        );

        let n: f64 = 3_f64.sqrt() / 3.;
        let normal = sphere.normal_at(&Point4f::new(n, n, n));
        assert_eq!(normal, Vector4f::new(n, n, n));
        // The normal is a normalized vector.
        assert_eq!(normal.normalized(), normal);
    }

    /// The normal of a translated sphere.
    #[test]
    fn normal_translation() {
        let sphere = Sphere::new(ShapeProperties::with_transform(
            Box::new(BLACK),
            Material::default(),
            Matrix4f::identity().translate(0., 1., 0.),
        ));

        assert_eq!(
            sphere.normal_at(&Point4f::new(0., 1.70711, -FRAC_1_SQRT_2)),
            Vector4f::new(0., FRAC_1_SQRT_2, -FRAC_1_SQRT_2)
        );
    }

    /// The normal of a transformed sphere.
    #[test]
    fn normal_transform() {
        let sphere = Sphere::new(ShapeProperties::with_transform(
            Box::new(BLACK),
            Material::default(),
            Matrix4f::identity()
                .rotate_z(std::f64::consts::PI / 5.)
                .scale(1., 0.5, 1.),
        ));

        assert_eq!(
            sphere.normal_at(&Point4f::new(0., FRAC_1_SQRT_2, -FRAC_1_SQRT_2)),
            Vector4f::new(0., 0.97014, -0.24254)
        );
    }
}
