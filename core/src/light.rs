use crate::properties::{Color, Material};
use math::{Point4f, Vector4f};

const BLACK: (u8, u8, u8) = (0, 0, 0);

pub trait Light {
    fn lighting(
        &self,
        point: &Point4f,
        eye_v: &Vector4f,
        normal: &Vector4f,
        material: &Material,
        color: Color,
        shadowed: bool,
    ) -> Color;

    fn get_position(&self) -> &Point4f;
}

/// A light source with no size.
pub struct PointLight {
    position: Point4f,
    intensity: Color,
}

impl PointLight {
    /// Creates a new point light.
    pub fn new(position: Point4f) -> Self {
        Self {
            position,
            intensity: Color::from((255, 255, 255)),
        }
    }

    /// Creates a new point light with the given intensity.
    pub fn with_intensity(position: Point4f, intensity: Color) -> Self {
        Self {
            position,
            intensity,
        }
    }
}

impl Light for PointLight {
    /// Computes the lighting of the point, using the Phong reflection model. The `eye_v` vector
    /// is a vector from the point to the origin of the ray.
    fn lighting(
        &self,
        point: &Point4f,
        eye_v: &Vector4f,
        normal: &Vector4f,
        material: &Material,
        color: Color,
        shadowed: bool,
    ) -> Color {
        // Combine the surface color with the light's intensity.
        let effective_color = color * self.intensity;
        // Compute ambient contribution.
        let ambient = effective_color * material.ambient;
        if shadowed {
            return ambient;
        }

        // Find the direction to the light source.
        let mut light_v = &self.position - point;
        light_v.normalize();

        let mut diffuse = Color::from(BLACK);
        let mut specular = Color::from(BLACK);

        let light_dot_normal = light_v.dot(normal);
        if light_dot_normal >= 0. {
            // Compute diffuse color.
            diffuse = effective_color * material.diffuse * light_dot_normal;

            let reflect_v = (-&light_v).reflect(normal);
            let reflect_dot_eye = reflect_v.dot(eye_v);

            // Compute specular color.
            if reflect_dot_eye > 0. {
                let factor = reflect_dot_eye.powf(material.shininess);
                specular = self.intensity * material.specular * factor;
            }
        }

        ambient + diffuse + specular
    }

    fn get_position(&self) -> &Point4f {
        &self.position
    }
}

#[cfg(test)]
mod tests {
    use crate::light::{Light, PointLight};
    use crate::properties::{Color, Material};
    use math::{Point4f, Vector4f};
    use std::f64::consts::FRAC_1_SQRT_2;

    /// The light is in front of the surface.
    #[test]
    fn lighting() {
        let material = Material::default();
        let point_light = PointLight::new(Point4f::new(0., 0., -10.));
        let point = Point4f::new(0., 0., 0.);
        let eye_v = Vector4f::new(0., 0., -1.);
        let normal = Vector4f::new(0., 0., -1.);
        let color = Color::from((255, 255, 255));

        // The eye is between the light and the surface.
        assert_eq!(
            point_light.lighting(&point, &eye_v, &normal, &material, color, false),
            Color {
                r: 1.9,
                g: 1.9,
                b: 1.9
            }
        );
        // The eye is between the light and the surface. The point is shadowed.
        assert_eq!(
            point_light.lighting(&point, &eye_v, &normal, &material, color, true),
            Color {
                r: 0.1,
                g: 0.1,
                b: 0.1
            }
        );
        assert_eq!(
            point_light.lighting(
                &point,
                &Vector4f::new(0., FRAC_1_SQRT_2, -FRAC_1_SQRT_2),
                &normal,
                &material,
                color,
                false
            ),
            Color {
                r: 1.,
                g: 1.,
                b: 1.,
            }
        );

        let point_light = PointLight::new(Point4f::new(0., 10., -10.));
        assert_eq!(
            point_light.lighting(&point, &eye_v, &normal, &material, color, false),
            Color {
                r: 0.7364,
                g: 0.7364,
                b: 0.7364,
            }
        );
        assert_eq!(
            point_light.lighting(
                &point,
                &Vector4f::new(0., -FRAC_1_SQRT_2, -FRAC_1_SQRT_2),
                &normal,
                &material,
                color,
                false
            ),
            Color {
                r: 1.6364,
                g: 1.6364,
                b: 1.6364,
            }
        );
    }

    /// The light is behind the surface.
    #[test]
    fn lighting_behind_surface() {
        let point_light = PointLight::new(Point4f::new(0., 0., 10.));
        let material = Material::default();
        let normal = Vector4f::new(0., 0., -1.);
        let color = Color::from((255, 255, 255));

        assert_eq!(
            point_light.lighting(
                &Point4f::new(0., 0., 0.),
                &Vector4f::new(0., 0., -1.),
                &normal,
                &material,
                color,
                false
            ),
            Color {
                r: 0.1,
                g: 0.1,
                b: 0.1,
            }
        );
    }
}
