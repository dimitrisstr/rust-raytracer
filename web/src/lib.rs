use crate::web_sys::{CanvasRenderingContext2d, HtmlTextAreaElement};
use core::canvas::Canvas;
use core::scene_parser::SceneParser;
use seed::{prelude::*, *};
use web_sys::HtmlCanvasElement;

const SAMPLE_SCENE: &str = "---
- define: &pi_div_2 1.5707963267948966
- define: &pi_div_3 1.0471975511965976
- define: &pi_div_4 0.7853981633974483
- define: &n_pi_div_3 -1.0471975511965976

# ===============
# The camera.
# ===============
- add: camera
  width: 920
  height: 920
  field_of_view: *pi_div_3
  from: [0.0, 1.5, -5.0]
  to: [0, 1.0, 0.0]
  up: [0.0, 1.0, 0.0]

# ===============
# Light.
# ===============
- add: light
  at: [-3.0, 10.0, -10.0]
  intensity: [1.0, 1.0, 1.0]

# ===============
# Floor.
# ===============
- add: plane
  material: &wall_material
    diffuse: 0.9
    specular: 0.0
  pattern: &wall_pattern
    # Pattern level 1.
    type: checkers
    transform:
      - [scale, 0.5, 0.5, 0.5]
    pattern1:
      # Pattern level 2.
      color: [1.0, 1.0, 1.0]
    pattern2:
      # Pattern level 2.
      color: [0.0, 0.0, 0.0]

# ===============
# Left wall.
# ===============
- add: plane
  transform:
    - [rotate_z, *pi_div_2]
    - [rotate_y, *pi_div_3]
    - [translate, 0.0, 0.0, 5.0]
  material: *wall_material
  pattern: *wall_pattern

# ===============
# Right wall.
# ===============
- add: plane
  transform:
    - [rotate_z, *pi_div_2]
    - [rotate_y, *n_pi_div_3]
    - [translate, 0.0, 0.0, 5.0]
  material: *wall_material
  pattern: *wall_pattern

# ===============
# Left sphere.
# ===============
- add: sphere
  transform:
    - [translate, -2.0, 1.0, 0.5]
  material:
    diffuse: 0.7
    specular: 0.3
  pattern:
    # Pattern level 1.
    type: stripes
    transform:
      - [scale, 0.1, 0.1, 0.1]
      - [rotate_y, *n_pi_div_3]
    pattern1:
      # Pattern level 2
      color: [0.0, 0.2, 0.5]
    pattern2:
      # Pattern level 2.
      color: [0.2, 0.2, 7.0]

# ===============
# Middle sphere.
# ===============
- add: sphere
  transform:
    - [scale, 0.5, 1.0, 1.5]
    - [translate, 0.0, 1.0, 0.5]
  material:
    diffuse: 0.9
    specular: 0.2
  pattern:
    type: ring
    transform:
      - [scale, 0.1, 0.1, 0.1]
      - [rotate_y, *pi_div_4]
    pattern1:
      color: [0.0, 0.8, 0.2]
    pattern2:
      color: [0.1, 1.0, 0.5]

# ===============
# Right sphere.
# ===============
- add: sphere
  transform:
    - [scale, 1.0, 0.5, 1.5]
    - [translate, 1.7, 1.0, 0.5]
  material:
    diffuse: 0.7
    specular: 0.8
  pattern:
    type: gradient
    transform:
      - [scale, 2.0, 2.0, 2.0]
      - [translate, 1.0, 0.0, 0.0]
    pattern1:
      color: [1.0, 1.0, 0.0]
    pattern2:
      color: [0.8, 0.3, 0.0]
...
";

fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model::default()
}

struct Model {
    html_canvas: ElRef<HtmlCanvasElement>,
    text_area: ElRef<HtmlTextAreaElement>,
    error_message: Option<String>,
}

impl Default for Model {
    fn default() -> Self {
        Self {
            html_canvas: ElRef::<HtmlCanvasElement>::default(),
            text_area: ElRef::<HtmlTextAreaElement>::default(),
            error_message: None,
        }
    }
}

enum Msg {
    Render,
}

#[allow(clippy::needless_pass_by_value)]
fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        Msg::Render => {
            draw(model);
        }
    }
}

fn draw(model: &mut Model) {
    match SceneParser::load(&model.text_area.get().expect("Get text area").value()) {
        Ok((camera, scene)) => {
            let canvas = camera.render(&scene);

            let html_canvas = model.html_canvas.get().expect("Get canvas");
            html_canvas.set_width(canvas.width() as u32);
            html_canvas.set_height(canvas.height() as u32);

            let ctx = canvas_context_2d(&html_canvas);
            draw_image(&ctx, &canvas);
            model.error_message = None;
        }
        Err(e) => {
            model.error_message = Some(e.to_string());
        }
    }
}

fn draw_image(ctx: &CanvasRenderingContext2d, canvas: &Canvas) {
    for x in 0..canvas.width() {
        for y in 0..canvas.height() {
            let color: (u8, u8, u8) = canvas.get_pixel(x, y).into();
            ctx.set_fill_style(&JsValue::from(format!(
                "rgb({},{},{})",
                color.0, color.1, color.2
            )));
            ctx.fill_rect(x as f64, y as f64, 1., 1.);
        }
    }
}

fn view(model: &Model) -> Node<Msg> {
    div![
        div![
            button![
                C!("noselect blue"),
                "Render",
                ev(Ev::Click, |_| Msg::Render)
            ],
            div![
                C!(if model.error_message.is_some() {
                    "error-label"
                } else {
                    "hidden"
                }),
                model.error_message.as_ref().unwrap_or(&String::default())
            ]
        ],
        div![
            C!("flex-parent-element"),
            div![
                C!("flex-child-element"),
                textarea![
                    attrs! {
                        At::SpellCheck => "false"
                    },
                    el_ref(&model.text_area),
                    SAMPLE_SCENE
                ]
            ],
            div![
                C!("flex-child-element"),
                canvas![el_ref(&model.html_canvas),]
            ],
        ]
    ]
}

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}
